# Student Lab Orchestration

A fork of the pythonlaborchestrator specialized for the student labs in uppsala cellar

## Features

## Installation

    pip install student_lab_orchestration --index-url https://gitlab.com/api/v4/projects/<gitlab-project-id>/packages/pypi/simple

## Usage

    student_lab_orchestration --help 

## Development

    git clone gitlab.com/None/student-lab-orchestration

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://None.gitlab.io/student-lab-orchestration](https://None.gitlab.io/student-lab-orchestration) or [student-lab-orchestration.gitlab.io](student_lab_orchestration.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



