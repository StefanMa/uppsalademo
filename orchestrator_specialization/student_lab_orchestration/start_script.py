"""
Main script to start the orchestrator with a dash GUI, configure the settings and inject the Greifswald worker
implementation. Uses the preliminary database implementation
"""

from laborchestrator.old_dash_app import SMDashApp, OrchestratorInterface
from laborchestrator.orchestrator_implementation import Orchestrator
from platform_status_db.larastatus.status_db_implementation import StatusDBImplementation
from student_lab_orchestration.cellar_worker import CellarWorker
import time
import sys
sys.path.append('/home/stefan/workspace/uppsalademo/orchestrator_specialization/student_lab_orchestration/')
import processes

# create an orchestrator with our worker and database implementation
orchestrator = Orchestrator(reader='PythonLab', worker_type=CellarWorker)
#database_client = StatusDBImplementation()
#orchestrator.inject_db_interface(database_client)
#orchestrator.start_sila_interface()

# checks whether all interface methods are implemented
assert isinstance(orchestrator, OrchestratorInterface)

# create and run dash app until eternity
dash_app = SMDashApp(orchestrator, port=8050, process_module=processes)
dash_app.run()
while True:
    time.sleep(1)
