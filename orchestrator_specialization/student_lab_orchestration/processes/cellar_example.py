
from pythonlab.resource import DynamicLabwareResource as ReagentResource
from os import path
import sys
sys.path.append(path.dirname(__file__))
from basic_cellar_process import BasicCellarProcess


class CellarExampleProcess(BasicCellarProcess):
    def __init__(self, priority=10):  # 0 has highest priority
        super().__init__(priority=priority, num_plates=2, process_name="CellarProcess")

    def process(self):
        # todo change to the actual names
        pipet_protocol_1 = "first_protocol"
        picture_take = "microscope_protocol"
        pipet_protocol_2 = "second_protocol"

        # the process steps
        # iterate oder both containers
        for cont in self.containers:
            # todo until the liquid handler is integrated, there needs to be a human involved
            # self.pipetter.executeProtocol(protocol=pipet_protocol_1, container=cont)
            self.human.do_task(labware=cont, message=f"Have the liquid handler run protocol {pipet_protocol_1}")
            self.human.do_task(labware=cont, message="ensure, microscope is in loading position")
            self.robot_arm.move(cont, target_loc=self.microscope)
        self.human.do_task(labware=self.containers, message=f"focus the microscope")
        for cont in self.containers:
            if cont == self.containers[0]:
                self.microscope.load(cont)
                self.microscope.run_protocol(protocol="auto_focus", labware=cont)
                self.microscope.run_protocol(protocol=picture_take, labware=cont)
                self.microscope.unload(cont)
            else:
                self.microscope_pool.load(cont)
                self.microscope_pool.run_protocol(protocol=picture_take, labware=cont)
                self.microscope_pool.unload(cont)
            self.robot_arm.move(cont, target_loc=self.pipetter)
            # self.pipetter.executeProtocol(protocol=pipet_protocol_2, container=cont)
            self.human.do_task(labware=cont, message=f"Have the liquid handler run protocol {pipet_protocol_2}")

