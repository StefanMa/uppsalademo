from abc import ABC

from pythonlab.resources.services.moving import MoverServiceResource
from pythonlab.resources.services.microscope import MicroscopeServiceResource
from pythonlab.resources.services.liquid_handling import LiquidHandlerServiceResource
from pythonlab.resources.services.human import HumanServiceResource
from pythonlab.resource import LabwareResource as ContainerResource
from pythonlab.resource import DynamicLabwareResource as ReagentResource
from pythonlab.process import PLProcess


class BasicCellarProcess(PLProcess, ABC):
    def __init__(self, process_name: str, num_plates: int = 0, priority=10):  # 0 has highest priority

        self.num_mw_plates = num_plates  # different number
        self.name = process_name

        super().__init__(priority=priority)

    def create_resources(self):
        self.robot_arm = MoverServiceResource(proc=self, name="Dorna")
        self.pipetter = LiquidHandlerServiceResource(proc=self, name="Flow", capacity=3)
        self.human = HumanServiceResource(proc=self, name='Student', capacity=1)
        self.microscope = MicroscopeServiceResource(proc=self, name="Squid", capacity=1)
        self.microscope2 = MicroscopeServiceResource(proc=self, name="Squid2", capacity=1)
        self.microscope_pool = MicroscopeServiceResource(proc=self, name=None, capacity=1)
        self.containers = [ContainerResource(proc=self, name=f"{self.name}_cont_{cont}", lidded=True, filled=False)
                           for cont in range(self.num_mw_plates)]

    def init_service_resources(self):
        # setting start position of containers
        super().init_service_resources()
        for i, cont in enumerate(self.containers):
            cont.set_start_position(self.pipetter, i)

    def process(self):
        raise NotImplementedError
