"""
Am Implementation/Subclass of the general WorkerInterface fitted to the Uppsala student lab
"""
import logging
import traceback
from typing import Dict, Optional, NamedTuple, Any
from numpy import random

from laborchestrator.engine import ScheduleManager
from laborchestrator.engine.worker_interface import WorkerInterface, ObservableProtocolHandler, Observable, DummyHandler
from laborchestrator.structures import ProcessStep, MoveStep, ContainerInfo, SchedulingInstance
from laborchestrator.database_integration import StatusDBInterface
from sila2.client import SilaClient
from devices import (
    UppsalaHuman, UppsalaSquid, UppsalaDorna, UppsalaFlowRobot, DeviceInterface
)

devices: Dict[str, DeviceInterface] = dict(
    Dorna=UppsalaDorna(name='Dorna'),
    Squid=UppsalaSquid(name='Squid'),
    Flow=UppsalaFlowRobot(name='Flow'),
    Human=UppsalaHuman(name="Human"),
)


class CellarWorker(WorkerInterface):
    clients: Dict[str, SilaClient]
    jssp: SchedulingInstance
    clients: Dict[str, SilaClient] = {}

    def __init__(self, jssp: SchedulingInstance, schedule_manager: ScheduleManager, db_client: StatusDBInterface):
        super(CellarWorker, self).__init__(jssp, schedule_manager, db_client)
        self.clients = {}

    def simulate_process_step(self, step_id: str, device: str, device_kwargs: Dict[str, Any]) -> Observable:
        observable = DummyHandler(duration=max(self.jssp.definite_step_by_id[step_id].duration - .5, .1))
        observable.run_protocol(None)
        return observable

    def execute_process_step(self, step_id: str, device: str, device_kwargs: Dict[str, Any]):
        try:
            step = self.jssp.step_by_id[step_id]
            # find the server for the device if no client exists
            client = self.get_client(device)
            cont = None

            # try to update the runtime container info from the database
            for cont_name in reversed(step.cont_names):
                try:
                    cont = self.jssp.container_info_by_name[cont_name]
                    # first try to find the info by barcode
                    """cont_info = self.db_client.get_cont_info_by_barcode(cont.barcode)
                    if not cont_info:
                        # if the barcode failed, try to find the information by location
                        cont_info = self.db_client.get_container_at_position(cont.current_device, cont.current_pos)
                        if not cont_info:
                            cont_info = cont
                    # copy information to the runtime environment. The database is considered more reliable
                    cont.barcode = cont_info.barcode
                    cont.current_pos = cont_info.current_pos
                    cont.current_device = cont_info.current_device
                    cont.lidded = cont_info.lidded
                    cont.lid_site = cont_info.lid_site
                    cont.filled = cont_info.filled"""
                except Exception as ex:
                    logging.error(f"Failed to retrieve container information from db: {ex}")

            logging.debug(f"executing step {step_id} on container {cont}")
            # get the custom execution command
            handler = devices[device].get_SiLA_handler(step, cont)
            observable_command_info = handler.run_protocol(client, **step.data)
            print(f"started executing step {step_id} on {device}")
            return observable_command_info
        except TimeoutError as ex:
            logging.error(f"Could not execute step {step_id} on device {device}")

    def process_step_finished(self, step_id: str, result: Optional[NamedTuple]):
        """
        Collection of stuff that needs to be saved after certain jobs finished successfully
        """
        super().process_step_finished(step_id, result)
        """"
        try:
            job = self.jssp.step_by_id[step_id]
            container = self.jssp.container_info_by_name[job.cont]
            response = job.result
            if isinstance(job, MoveStep):
                if 'lidded' in job.data:
                    if job.data['lidded']:
                        # in case of lidding (if it was necessary)... was save the status anyway
                        self.db_client.lidded_container(container, None, None)
                    else:
                        # in case of unlidding (if it was necessary)
                        if 'LidPosition' in dir(response):
                            # in that case a unlidding really happened
                            print(response)
                            self.db_client.unlidded_container(container, 'Hotel_1', response.LidPosition)
            if 'Barcode' in dir(response):
                if container.barcode and not container.barcode == response.Barcode:
                    logging.warning(f"Container {container}' barcode was read as {response.Barcode}, which differs"
                                    f"from what is is supposed to have({container.barcode}). Working with new one.")
                container.barcode = response.Barcode
                logging.debug(f"Read barcode as {response.Barcode}")
                self.db_client.set_barcode(container)
        except Exception as ex:
            logging.warning(f"Evaluation of step {step_id} with result {result} went wrong: {ex}|{traceback.print_exc()}")
        """

    def get_client(self, device: str):
        if self.simulation_mode:
            return None
        if device not in devices:
            return None
        try:
            if device not in self.clients:
                #todo remove that as soon the sila-servers run
                if device not in ["Human", "Dorna", "Squid"]:
                    return None
                try:
                    # try insecure connection first
                    client = SilaClient.discover(insecure=True, server_name=device, timeout=5)
                except:
                    print("insecure connection did not work. trying secure connection")
                    logging.debug("insecure connection did not work. trying secure connection")
                    cert = self.db_client.get_server_certificate(device_name=device).encode()
                    client = SilaClient.discover(root_certs=cert, server_name=device, timeout=5)
                self.clients[device] = client
        except TimeoutError as ex:
            logging.warning(f"Could not connect to device {device}")
            return None
        return self.clients[device]

    def determine_destination_position(self, step: MoveStep) -> Optional[int]:
        dest_name = step.target_device.name
        # consider wishes for positioning of plates
        if step.pref_dest_pos:
            if self.db_client.get_container_at_position(dest_name, step.pref_dest_pos):
                logging.warning(f"Container {step.cont} should go to slot {step.pref_dest_pos}, but there is"
                                f"{self.db_client.get_container_at_position(dest_name, step.pref_dest_pos)}")
                logging.warning("We still try to put it at the intended position.")
                return step.pref_dest_pos
            else:
                return step.pref_dest_pos
        else:
            # try to get the client
            dest_client = self.get_client(dest_name)
            if dest_client:
                # get the device intervace
                dest_device = devices[dest_name]
                # ask the destination device for its next free position
                next_free_slot = dest_device.get_next_free_position(dest_client)
                print(f"client({dest_device}) says, next free slot in {dest_name} in {next_free_slot}")
                return next_free_slot
            else:
                # fall back to the default approach
                return super().determine_destination_position(step)
