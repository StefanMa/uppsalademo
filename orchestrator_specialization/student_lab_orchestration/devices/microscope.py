from lara_processes.devices.device_interface import DeviceInterface
from laborchestrator.engine.worker_interface import ObservableProtocolHandler
from pythonlab.resources.services.liquid_handling import LiquidHandlerServiceResource
from laborchestrator.structures import ProcessStep, ContainerInfo
from sila2.client import ClientObservableCommandInstance
try:
    from microscope_server.generated.client import Client as SquidClient
except ModuleNotFoundError:
    from sila2.client import SilaClient as SquidClient
from typing import Union, Optional
from sila2.framework import CommandExecutionStatus
import time


class UppsalaSquid(DeviceInterface):
    def __init__(self, name, capacity=3):
        super(UppsalaSquid, self).__init__(['executeProtocol'], LiquidHandlerServiceResource, name,
                                              capacity=capacity)

    def get_SiLA_handler(self, job: ProcessStep, cont: Optional[ContainerInfo]):
        if job.function == "run_protocol":
            class ProtocolHandler(ObservableProtocolHandler):
                def run_protocol(self, client: SquidClient, **kwargs):
                    return client.ProtocolController.RunProtocol(
                        ProtocolName=job.data['protocol']
                    )
            return ProtocolHandler()

        if job.function == "load":
            class ProtocolHandler(ObservableProtocolHandler):
                def run_protocol(self, client: SquidClient, **kwargs):
                    return client.StageController.LeaveLoadingPosition(Speed=1)
            return ProtocolHandler()

        if job.function == "unload":
            class ProtocolHandler(ObservableProtocolHandler):
                def run_protocol(self, client: SquidClient, **kwargs):
                    return client.StageController.GoToLoading()
            return ProtocolHandler()

    def get_next_free_position(self, client: SquidClient) -> Union[int, None]:
        return None
