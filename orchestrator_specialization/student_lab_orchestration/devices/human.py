from lara_processes.devices.device_interface import DeviceInterface
from laborchestrator.engine.worker_interface import ObservableProtocolHandler
from pythonlab.resources.services.human import HumanServiceResource
from laborchestrator.structures import ProcessStep, ContainerInfo

try:
    from human_server.generated.client import Client as HumanClient
except ModuleNotFoundError:
    from sila2.client import SilaClient as HumanClient
from sila2.framework import CommandExecutionStatus, SilaAnyType
from datetime import timedelta


class UppsalaHuman(DeviceInterface):
    def __init__(self, name):
        super(UppsalaHuman, self).__init__(['ask_for_ok', 'request_number', 'do_task'],
                                              HumanServiceResource, name, capacity=100)

    def get_SiLA_handler(self, job: ProcessStep, cont: ContainerInfo):
        if job.function == 'ask_for_ok':
            class HumanHandler(ObservableProtocolHandler):
                def run_protocol(self, human_client: HumanClient, **kwargs):
                    return human_client.HumanController.CustomCommand(
                        Description="Say OK",
                        ResponseStructure=SilaAnyType(type_xml="<DataType><Basic>Real</Basic></DataType>", value=1
                                                      ))
            return HumanHandler()

        if job.function == 'request_number':
            class HumanHandler(ObservableProtocolHandler):
                def run_protocol(self, human_client: HumanClient, **kwargs):
                    return human_client.HumanController.CustomCommand(
                        Description=kwargs["message"],
                        ResponseStructure=SilaAnyType(type_xml="<DataType><Basic>Integer</Basic></DataType>", value=20
                                                      )
                    )

        if job.function == 'do_task':
            class HumanHandler(ObservableProtocolHandler):
                def run_protocol(self, human_client: HumanClient, **kwargs):
                    return human_client.HumanController.CustomCommand(
                        Description=kwargs["message"],
                        ResponseStructure=SilaAnyType(type_xml="<DataType><Basic>String</Basic></DataType>",
                                                      value="Completed"
                                                      )
                    )

            return HumanHandler()

    def get_next_free_position(self, client: HumanClient):
        return None
