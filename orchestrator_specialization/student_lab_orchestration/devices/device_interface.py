from __future__ import annotations
from typing import List, Dict, Type, Union, Any
from abc import ABC, abstractmethod
from pythonlab.resource import ServiceResource
from laborchestrator.structures import ProcessStep, ContainerInfo
from laborchestrator.engine.worker_interface import ObservableProtocolHandler


class DeviceInterface(ABC):
    def __init__(self, functions, service_type: Type[ServiceResource], name, capacity: int = 1):
        self.functions = functions
        self.service_type = service_type
        self.name = name
        self.capacity = capacity

    @abstractmethod
    def get_SiLA_handler(self, job: ProcessStep, cont: ContainerInfo) -> ObservableProtocolHandler:
        """
        Provides a SiLACommandHandler, that gives the SiLA protocol for the specified function on this device
        and provides functions to enquiry the status and remaining time of this protocol
        :param job:
        :param cont:
        :return: a ObservableProtocolHandler
        """

    @abstractmethod
    def get_next_free_position(self, client) -> Union[int, None]:
        """
        Utility function to enable dynamic positioning of containers.
        :return: the position index (starting at 0) or None if no position is free
        """
