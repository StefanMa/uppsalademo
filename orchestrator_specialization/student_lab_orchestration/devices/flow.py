from lara_processes.devices.device_interface import DeviceInterface
from laborchestrator.engine.worker_interface import ObservableProtocolHandler
from pythonlab.resources.services.liquid_handling import LiquidHandlerServiceResource
from laborchestrator.structures import ProcessStep, ContainerInfo
from sila2.client import ClientObservableCommandInstance
try:
    from bravo_server.generated.client import Client as BravoClient
except ModuleNotFoundError:
    from sila2.client import SilaClient as BravoClient
from typing import Union, Optional
from sila2.framework import CommandExecutionStatus
import time


class UppsalaFlowRobot(DeviceInterface):
    def __init__(self, name, capacity=1):
        super(UppsalaFlowRobot, self).__init__(['executeProtocol'], LiquidHandlerServiceResource, name,
                                               capacity=capacity)

    def get_SiLA_handler(self, job: ProcessStep, cont: Optional[ContainerInfo]):
        if job.function == "executeProtocol":
            class ProtocolHandler(ObservableProtocolHandler):
                #def run_protocol(self, client: BravoClient, **kwargs) -> ClientObservableCommandInstance:
                #    return client.ProtocolServicer.RunProtocol(job.data['method'])

                def _protocol(self, client, **kwargs):
                    self._status = CommandExecutionStatus.running
                    print(f'Executing protocol on {cont} ...')
                    time.sleep(3)
                    self._status = CommandExecutionStatus.finishedSuccessfully
                    print("... pipetter done")

            return ProtocolHandler()
        return None

    def get_next_free_position(self, client: BravoClient) -> Union[int, None]:
        free = client.RobotInteractionService.CanReceiveContainer.get()
        if True in free:
            return free.index(True)
        else:
            return None
