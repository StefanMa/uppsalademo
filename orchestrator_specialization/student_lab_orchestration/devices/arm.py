from lara_processes.devices.device_interface import DeviceInterface
from laborchestrator.engine.worker_interface import ObservableProtocolHandler
from pythonlab.resources.services.moving import MoverServiceResource
from laborchestrator.structures import ProcessStep, ContainerInfo, MoveStep
try:
    from dorna_server.generated.client import Client as ArmClient
except ModuleNotFoundError:
    from sila2.client import SilaClient as ArmClient


class UppsalaDorna(DeviceInterface):
    def __init__(self, name):
        super(UppsalaDorna, self).__init__(['move'], MoverServiceResource, name)

    def get_SiLA_handler(self, step: ProcessStep, cont: ContainerInfo):
        if step.function == 'move':
            class MoveHandler(ObservableProtocolHandler):
                def run_protocol(self, client: ArmClient, **kwargs):
                    origin = [cont.current_device, cont.current_pos]
                    destination = [step.target_device.name, step.destination_pos]
                    return client.RobotController.MovePlate(
                        OriginSite=origin,
                        DestinationSite=destination
                    )
            return MoveHandler()
        return None

    def get_next_free_position(self, client: ArmClient):
        return None
