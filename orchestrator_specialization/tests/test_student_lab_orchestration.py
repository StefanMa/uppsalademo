#!/usr/bin/env python
"""Tests for `student_lab_orchestration` package."""
# pylint: disable=redefined-outer-name
from student_lab_orchestration import __version__
from student_lab_orchestration.student_lab_orchestration_interface import GreeterInterface
from student_lab_orchestration.student_lab_orchestration_impl import HelloWorld

def test_version():
    """Sample pytest test function."""
    assert __version__ == "0.0.1"

def test_GreeterInterface():
    """ testing the formal interface (GreeterInterface)
    """
    assert issubclass(HelloWorld, GreeterInterface)

def test_HelloWorld():
    """ Testing HelloWorld class
    """
    hw = HelloWorld()
    name = 'yvain'
    assert hw.greet_the_world(name) == f"Hello world, {name} !"

