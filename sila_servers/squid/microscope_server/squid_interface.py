"""
This class handles the interaction between the SiLA server and the device API
"""

import time
import logging


class SquidInterface:

    def __init__(self):
        """

        """
        self.connect()
        self._in_loading_position = False  # this is more secure

    def connect(self) -> bool:
        """

        :return: Whether connection was established successfully
        """
        logging.info("Connected to squid hardware")
        # todo insert API call
        return True

    def run_protocol(self, filename: str):
        """
        Runs a protocol on the squid. This method finishes when the protocol is finished
        :param filename: location and name of the protocol file
        :return:
        """
        logging.debug(f"Starting protocol {filename}.")
        # todo insert API call
        time.sleep(7)
        print("....protocol finished.")

    def go_into_loading_position(self):
        """

        :return:
        """
        # todo insert API call
        print("moving into loading position ...")
        time.sleep(2)
        print("...done")
        self._in_loading_position = True

    def leave_loading_position(self):
        """

        :return:
        """
        # todo insert API call
        print("leaving loading position ...")
        time.sleep(2)
        print("...done")
        self._in_loading_position = False

    @property
    def is_in_loading_position(self) -> bool:
        return self._in_loading_position
