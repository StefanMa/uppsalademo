from dorna_internal.dorna_main import DornaInterface
from dorna_server.server import Server as DornaSilaServer
from dorna_internal.dorna_dash import DornaDashApp
import time


def main():
    print("Starting the Dorna interface, a SiLA server for it and a dash_app for visualization")

    # starts an interface to the robot
    dorna = DornaInterface()

    # starts a sila server accessing the dorna interface
    server = DornaSilaServer(dorna_interface=dorna)
    server.start_insecure(address='127.0.0.1', port=50063)

    # starts a visualization of the robots position network
    dash_app = DornaDashApp(dorna_interface=dorna, port=8055)
    dash_app.run()

    time.sleep(1)
    print('press q -> enter to stop')
    while True:
        if input() == 'q':
            break
    print("bye!")


if __name__ == '__main__':
    main()
