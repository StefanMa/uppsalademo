graph [
  node [
    id 0
    label "low_right"
    x 391.943621
    y -180.035702
    z 165.274953
  ]
  node [
    id 1
    label "low_mid"
    x 197.908175
    y -362.095948
    z 82.907054
  ]
  node [
    id 2
    label "high_mid"
    x 253.118656
    y -296.30496
    z 366.679695
  ]
  node [
    id 3
    label "Flow"
    x 0.1599585912438759
    y 0.05093680661090949
    z 0.6193774656996651
  ]
  node [
    id 4
    label "Squid"
    x 0.4102788021591476
    y 0.8763096407044407
    z 0.951485564792064
  ]
  node [
    id 5
    label "home"
    x 0.7022980508259606
    y 0.19547756486930046
    z 0.16895098885012227
  ]
  node [
    id 6
    label "sleep"
    x 0.5586482939538289
    y 0.9221972533822669
    z 0.4198037318489749
  ]
  node [
    id 7
    label "flow_1"
    x 0.8105160463971961
    y 0.8574323912118977
    z 0.6322462816884925
  ]
  node [
    id 8
    label "flow_2"
    x 0.39912955233242065
    y 0.11361772323210717
    z 0.3123388804149323
  ]
  node [
    id 9
    label "flow_3"
    x 0.4883237535617484
    y 0.9403651350722984
    z 0.6970274213589117
  ]
  node [
    id 10
    label "Squid_load"
    x 0.2512616902713224
    y 0.3291342775788597
    z 0.47815501944255157
  ]
  node [
    id 11
    label "Squid_safe"
    x 0.24677292295453512
    y 0.04831176808648874
    z 0.4296627174819493
  ]
  edge [
    source 0
    target 1
    dist 1
  ]
  edge [
    source 0
    target 11
    dist 1
  ]
  edge [
    source 1
    target 2
    dist 1
  ]
  edge [
    source 1
    target 3
    dist 1
  ]
  edge [
    source 2
    target 5
    dist 1
  ]
  edge [
    source 2
    target 11
    dist 1
  ]
  edge [
    source 3
    target 7
    dist 1
  ]
  edge [
    source 3
    target 8
    dist 1
  ]
  edge [
    source 3
    target 9
    dist 1
  ]
  edge [
    source 4
    target 10
    dist 1
  ]
  edge [
    source 4
    target 11
    dist 1
  ]
  edge [
    source 5
    target 6
    dist 1
  ]
]
