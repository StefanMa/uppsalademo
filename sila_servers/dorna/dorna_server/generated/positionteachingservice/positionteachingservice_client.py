# Generated by sila2.code_generator; sila2.__version__: 0.10.4
# -----
# This class does not do anything useful at runtime. Its only purpose is to provide type annotations.
# Since sphinx does not support .pyi files (yet?), this is a .py file.
# -----

from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any, Iterable, Optional

    from positionteachingservice_types import (
        AddConnection_Responses,
        AddPosition_Responses,
        RemoveConnection_Responses,
        RemovePosition_Responses,
    )
    from sila2.client import ClientMetadataInstance, ClientUnobservableProperty

    from .positionteachingservice_types import Edge


class PositionTeachingServiceClient:
    """

    A feature enabling the teaching of movements to a robotic arm. You can teach and name positions as well as
    allow movements between positions. This creates a graph along which edges the robot will move.
    You can also remove positions and connections.

    """

    CurrentNetworkGraph: ClientUnobservableProperty[Any]
    """
    
            Gives the nodes and edges of the current movement graph.
        
    """

    def AddPosition(
        self, PositionIdentifier: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> AddPosition_Responses:
        """
        Adds the current position to the graph
        """
        ...

    def AddConnection(
        self, Edge: Edge, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> AddConnection_Responses:
        """

        Allows the robot to move straight between two positions.

        """
        ...

    def RemovePosition(
        self, PositionIdentifier: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> RemovePosition_Responses:
        """
        Removes the current position to the graph
        """
        ...

    def RemoveConnection(
        self, Edge: Edge, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> RemoveConnection_Responses:
        """

        Forbids the robot to move straight between two positions.

        """
        ...
