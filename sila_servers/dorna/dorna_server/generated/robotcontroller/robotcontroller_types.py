# Generated by sila2.code_generator; sila2.__version__: 0.10.4
from __future__ import annotations

from typing import Any, NamedTuple


class PickPlate_Responses(NamedTuple):
    pass


class PlacePlate_Responses(NamedTuple):
    pass


class SetSpeed_Responses(NamedTuple):
    pass


class SetAcceleration_Responses(NamedTuple):
    pass


class EmergencyStop_Responses(NamedTuple):
    pass


class MoveToPosition_Responses(NamedTuple):
    pass


class MovePlate_Responses(NamedTuple):
    pass


Site = Any
