"""
Main class to represent a dorna robotic arm
"""
from typing import Optional, List

from dorna_internal.graph_manager import GraphManager, Point3D
from numpy.random import rand
from threading import Lock
import time
from dorna2 import Dorna


class DornaInterface:
    current_position: Optional[str] = None
    next_positions: List[str]
    hardware_interface: Dorna

    def __init__(self, start_simulating: bool = True):
        self.is_in_simulation_mode = start_simulating
        print("Hello World")
        self.graph_manager = GraphManager()
        self.next_positions = []
        # ensures, that only one movement or gripping command is executed at a time
        self.move_lock = Lock()
        self.init_connection()

    def init_connection(self):
        """
        Sets up the hardware connection
        :return:
        """
        if not self.is_in_simulation_mode:
            self.hardware_interface = Dorna()
            self.hardware_interface.connect(host='192.168.88.31')

    def stop_moving(self):
        """

        :return:
        """
        if not self.is_in_simulation_mode:
            # makes the robot immediately stop all movements
            self.hardware_interface.halt()
        print("Immediately stopped moving")
        self.next_positions = []
        # wait for movement command to finish and nullify the current position (we do not know where we stopped)
        with self.move_lock:
            self.current_position = None

    def set_speed(self, new_speed: float):
        """

        :param new_speed:
        :return:
        """
        # TODO insert API call

    def _move_to_nearest_known_position(self):
        """

        :return:
        """
        nearest = self.graph_manager.get_nearest_position(self.current_coordinates)
        print(f"nearest position is {nearest}")
        self._move_straight_to_position(nearest)

    def add_current_position(self, identifier: str):
        """

        :param identifier:
        :return:
        """
        self.graph_manager.add_position(identifier=identifier, pos=self.current_coordinates)

    def move_to_position(self, target_position: str):
        """

        :param target_position:
        :return:
        """
        with self.move_lock:
            # if the current position is unknown, move to nearest known position
            if not self.current_position:
                self._move_to_nearest_known_position()
            # get the path to the target location
            path = self.graph_manager.get_shortest_path(self.current_position, target_position)
            for identifier in path:
                self.next_positions.append(identifier)
            # move from point to point
            while self.next_positions:
                self._move_straight_to_position(self.next_positions[0])
                # to improve visualization we keen the position in the list until the movement finished
                self.next_positions.pop(0)

    def grip_close(self):
        """

        :return:
        """
        with self.move_lock:
            # TODO insert API call
            print("closing gripper")
            time.sleep(.5)

    def grip_open(self):
        """

        :return:
        """
        with self.move_lock:
            print('opening gripper')
            # TODO insert API call
            time.sleep(.5)

    def _move_straight_to_position(self, identifier: str):
        print(f"moving to {identifier}")
        coord = self.graph_manager.get_coordinates(identifier)
        self._move_to_coordinates(coord)
        self.current_position = identifier

    def _move_to_coordinates(self, target: Point3D):
        """

        :param target:
        :return:
        """
        if self.is_in_simulation_mode:
            time.sleep(2)
        else:
            self.hardware_interface.play(cmd='lmove', x=target.x, y=target.y, z=target.z, rel=0)

    @property
    def current_coordinates(self) -> Point3D:
        if self.is_in_simulation_mode:
            return Point3D(rand(), rand(), rand())
        else:
            pose = self.hardware_interface.get_pose()
            x, y, z = pose[0:3]
            return Point3D(x, y, z)

    def __del__(self):
        if not self.is_in_simulation_mode:
            self.hardware_interface.close()
