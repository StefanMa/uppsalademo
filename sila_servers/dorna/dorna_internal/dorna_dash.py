"""
Dash visualization of what the position graph of a robotic arm looks like and what its current movements are.
"""
import traceback

from dash import dcc, html, no_update
from dash.dependencies import State, Input, Output
from dash_extensions.enrich import MultiplexerTransform, DashProxy
import dash_interactive_graphviz
from .dorna_main import DornaInterface
from threading import Thread
import logging
from graphviz import Graph


class DornaDashApp:
    app: DashProxy
    robot: DornaInterface

    def __init__(self, dorna_interface: DornaInterface, port=8055):
        self.dot_string = "digraph {\n\thello -> world\n}"
        self.robot = dorna_interface
        self.app = DashProxy("Dorna Movement Visualization", prevent_initial_callbacks=True,
                             transforms=[MultiplexerTransform()])
        self.p = Thread(target=self.app.run_server, daemon=True, kwargs=dict(debug=False, port=port))

        self.app.layout = html.Div(children=[
            html.H1(children='Current Graph:'),
            dash_interactive_graphviz.DashInteractiveGraphviz(
                id="network", engine='dot',
                dot_source=self.dot_string
            ),
            html.Div(id='node', children='No node selected'),
            dcc.Interval(
                id='interval-component',
                interval=500,
                n_intervals=0
            ),
            ])

        @self.app.callback(
            Output(component_id='network', component_property='dot_source'),
            Input(component_id='interval-component', component_property='n_intervals')
        )
        def refresh_graph(n_intervals):
            try:
                nx_graph = self.robot.graph_manager.g
                dot = Graph(comment="Position Network")
                for name, data in nx_graph.nodes(data=True):
                    color = 'grey'
                    if name in self.robot.next_positions:
                        color = 'orange'
                        if name == self.robot.next_positions[-1]:
                            color = 'red'
                    if name == self.robot.current_position:
                        color = 'yellow'
                    dot.node(name, label=name, color=color, style='filled')
                for u, v , data in nx_graph.edges(data=True):
                    dot.edge(u, v, label=f"d = {data['dist']}")
                dot.format = "png"
                fig_str = str(dot)
                # only refresh if there was a change
                if self.dot_string == fig_str:
                    return no_update
                else:
                    return fig_str
            except Exception as ex:
                print(ex, traceback.print_exc())
                return no_update

        @self.app.callback(
            Output(component_id='node', component_property='children'),
            Input(component_id='network', component_property='selected_node'),
        )
        def select_operation(selected_node):
            return f"Coordinates: {self.robot.graph_manager.get_coordinates(selected_node)}"

    def run(self):
        if self.p.is_alive():
            logging.warning('Server is already running. Restarting server')
            self.stop()
        logging.getLogger('werkzeug').setLevel(logging.ERROR)
        self.p.start()

    def stop(self):
        print("Sorry, I don't know, how to stop")
