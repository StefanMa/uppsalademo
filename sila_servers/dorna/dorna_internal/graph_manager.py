"""

"""

from __future__ import annotations
import networkx as nx
from typing import NamedTuple, List
from math import dist
import os


class Point3D(NamedTuple):
    x: float
    y: float
    z: float

    def __str__(self):
        return f"({round(self.x, 4)}, {round(self.y, 4)}, {round(self.z, 4)})"

    def dist(self, p: Point3D):
        # the Euclidean distance
        return dist([self.x, self.y, self.z], [p.x, p.y, p.z])


FILENAME = "position_nx_graph.gml"


class GraphManager:
    g: nx.Graph

    def __init__(self):
        if os.path.exists(FILENAME):
            self.load(FILENAME)
        else:
            self.g = nx.Graph()

    def load(self, file_name: str):
        """
        Loads a networkx graph
        :param file_name:
        :return:
        """
        self.g = nx.read_gml(FILENAME)

    def save(self, filename: str):
        """
        Saves the current networkx graph
        :param filename:
        :return:
        """
        print(self.g)
        nx.write_gml(self.g, filename, stringizer=str)

    def add_position(self, identifier: str, pos: Point3D):
        """

        :param pos:
        :param identifier:
        :return:
        """
        self.g.add_node(identifier, x=pos.x, y=pos.y, z=pos.z)
        self.save(FILENAME)

    def add_connection(self, head: str, tail: str):
        """

        :param head:
        :param tail:
        :return:
        """
        # it is important (for this code) to consistently have a attribute called dist
        self.g.add_edge(tail, head, dist=1)
        self.save(FILENAME)

    def get_nearest_position(self, pos: Point3D) -> str:
        """

        :param pos:
        :return:
        """
        def criteria(n: str):
            return pos.dist(self.get_coordinates(n))
        sorted_positions = sorted(self.g.nodes, key=criteria)
        # the first one should be the nearest
        return sorted_positions[0]

    def get_coordinates(self, identifier: str) -> Point3D:
        """

        :param identifier:
        :return:
        """
        data = self.g.nodes[identifier]
        p = Point3D(data['x'], data['y'], data['z'])
        return p

    def get_shortest_path(self, start: str, target: str) -> List[str]:
        """

        :param start:
        :param target:
        :return:
        """
        path = nx.shortest_path(self.g, start, target, weight='dist')
        print("shortest path is:", path)
        # the first entry is the current node. we remove it.
        return path[1:]

    def remove_position(self, identifier: str):
        """

        :param identifier:
        :return:
        """
        if identifier in self.g.nodes:
            self.g.remove_node(identifier)
        self.save(FILENAME)

    def remove_connection(self, head: str, tail: str):
        """

        :param head:
        :param tail:
        :return:
        """
        if (tail, head) in self.g.edges:
            self.g.remove_edge(tail, head)
        self.save(FILENAME)
